######################
# Bottle Experiments #
# Coded by lavalleee #
######################

#Importing modules 
from bottle import route, run
from datetime import datetime

#URL /helloworld that prints "Hello World!"
@route('/helloworld')
def helloworld():
    return "Hello World! This is my web page."

#URL /date that outputs the date you're viewing this
@route('/date')
def date():
    date = datetime.today().strftime('%Y-%m-%d')
    return date


#Opens the bottle *bottle cork popping noise*
run(host='localhost', port=8080, debug=True)